/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment, Component } from 'react';
import { ScrollView, View, Text, StyleSheet, Image } from 'react-native';


class Blink extends Component {
  componentDidMount() {
    //Change each second
    setInterval(() => (
      this.setState(previousState => (
        {isShowingText : !previousState.isShowingText}
      ))
    ), 1000);
  }
  //State object
  state = {isShowingText: true};

  render() {
    if(!this.state.isShowingText) {
      return null;
    }
    else {
      return (
        <Text style={this.props.style}> {this.props.text} </Text>
      );
    }
  }
}

class Country extends Component {
  render() {
    return (
      <Text style={this.props.style}>
        I'm introducing,
        <Text style={styles.colorRed}> the flag of </Text>
        <Blink text={this.props.name}/>...
      </Text>
    );
  }
}

class ColoredBox extends Component {
  render () {
    return(
      <View style={
          {flex : 1,
           flexDirection : 'column',
           height: 300,
           backgroundColor : "aliceblue"}}>
        <View style={{flex:1}}/>
        <View style={{flex : 9, flexDirection : 'row'}}>
          <View style={{width : 193, height: 110, backgroundColor : "skyblue"}}/>
          <View style={{flex : 4, backgroundColor: "steelblue"}}/>
          <View style={{flex : 3, backgroundColor: "cadetblue"}}/>
        </View>
      </View>
    );
  }
}

export default class FirstApp extends Component {
  render () {
    let pic = {
      uri: "http://icons.iconarchive.com/icons/wikipedia/flags/256/CR-Costa-Rica-Flag-icon.png"
    };
    return (
      <Fragment>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.center}>
            <Text style={styles.boldText}>
              <Country name="Costa Rica" style={styles.colorBlue}/>
            </Text>
            <Image source={pic} style={styles.img}/>
          </View>
          <ColoredBox/>
        </ScrollView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  center : {
    flex : 1,
    justifyContent : "center", // Vertical centred
    alignItems : "center",   //Horizontal centred
    marginBottom : 25,
    marginTop : 20
  },
  img : {
    margin : 15,
    width : 193,
    height : 110
  },
  colorRed : {
    color : "red"
  },
  colorBlue : {
    color : "blue"
  },
  boldText : {
    fontWeight : "bold"
  }
});
